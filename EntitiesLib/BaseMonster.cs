﻿namespace EntitiesLib
{
    public class BaseMonster : IMyCloneable<BaseMonster>, ICloneable
    {
        public string Name { get; set; } = "Default Name";
        public int MeleeDamage { get; set; }
        public int Defence { get; set; }
        public int Health { get; set; }
        public int MoneyDrop { get; set; }

        public BaseMonster() { }

        public BaseMonster(BaseMonster monster)
        {
            Name = monster.Name;
            MeleeDamage = monster.MeleeDamage;
            Defence = monster.Defence;
            Health = monster.Health;
            MoneyDrop = monster.MoneyDrop;
        }

        public BaseMonster(string name, int meleeDamage, int defence, int health, int moneyDrop)
        {
            Name = name;
            MeleeDamage = meleeDamage;
            Defence = defence;
            Health = health;
            MoneyDrop = moneyDrop;
        }

        public virtual int Attack()
        {
            return MeleeDamage;
        }

        public object Clone()
        {
            return MyClone();
        }

        public virtual BaseMonster MyClone()
        {
            return new BaseMonster(this);
        }
    }
}
