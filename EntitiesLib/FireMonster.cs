﻿namespace EntitiesLib
{
    public class FireMonster : BaseMonster, IMyCloneable<FireMonster>, ICloneable
    {
        public int FireDamage { get; set; }
        public int TotalDamage { get; private set; }

        public FireMonster() { }

        public FireMonster(FireMonster monster)
            : base(monster.Name, monster.MeleeDamage, monster.Defence, monster.Health, monster.MoneyDrop)
        {
            FireDamage = monster.FireDamage;
            TotalDamage = MeleeDamage + FireDamage;
        }

        public FireMonster(string name, int meleeDamage, int defence, int health, int moneyDrop, int fireDamage)
            : base(name, meleeDamage, defence, health, moneyDrop)
        {
            FireDamage = fireDamage;
            TotalDamage = MeleeDamage + FireDamage;
        }

        public override int Attack()
        {
            return TotalDamage;
        }

        public override FireMonster MyClone()
        {
            return new FireMonster(this);
        }
    }
}