﻿namespace EntitiesLib
{
    public interface IMyCloneable<T>
    {
        T MyClone();
    }
}
