﻿namespace EntitiesLib
{
    public class PoisonMonster : BaseMonster, IMyCloneable<PoisonMonster>, ICloneable
    {
        public int PoisonDamage { get; set; }
        public int TotalDamage { get; private set; }

        public PoisonMonster() { }

        public PoisonMonster(PoisonMonster monster)
            : base(monster.Name, monster.MeleeDamage, monster.Defence, monster.Health, monster.MoneyDrop)
        {
            PoisonDamage = monster.PoisonDamage;
            TotalDamage = MeleeDamage + PoisonDamage;
        }

        public PoisonMonster(string name, int meleeDamage, int defence, int health, int moneyDrop, int poisonDamage)
            : base(name, meleeDamage, defence, health, moneyDrop)
        {
            PoisonDamage = poisonDamage;
            TotalDamage = MeleeDamage + PoisonDamage;
        }

        public override int Attack()
        {
            return TotalDamage;
        }

        public override PoisonMonster MyClone()
        {
            return new PoisonMonster(this);
        }
    }
}