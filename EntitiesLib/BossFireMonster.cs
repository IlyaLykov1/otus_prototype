﻿namespace EntitiesLib
{
    public class BossFireMonster : FireMonster, IMyCloneable<BossFireMonster>, ICloneable
    {
        public string Artefact { get; set; } = "Default Ring";

        public BossFireMonster() { }

        public BossFireMonster(BossFireMonster monster)
            : base(monster.Name, monster.MeleeDamage, monster.Defence, monster.Health, monster.MoneyDrop, monster.FireDamage)
        {
            Artefact = monster.Artefact;
        }

        public BossFireMonster(string name, int meleeDamage, int defence, int health, int moneyDrop, int fireDamage, string artefact)
            : base(name, meleeDamage, defence, health, moneyDrop, fireDamage)
        {
            Artefact = artefact;
        }

        public override BossFireMonster MyClone()
        {
            return new BossFireMonster(this);
        }
    }
}
