﻿using Xunit;

namespace EntitiesLib.UnitTests
{
    public class FireMonsterTests
    {
        [Fact]
        public void MyClone_ObjectsAreNotTheSameInstance()
        {
            // Arrange
            var expected = GetFireMonster();

            // Act
            var actual = expected.MyClone();

            // Assert
            Assert.NotSame(expected, actual);
        }

        [Fact]
        public void Clone_ObjectsAreNotTheSameInstance()
        {
            // Arrange
            var expected = GetFireMonster();

            // Act
            var actual = expected.Clone();

            // Assert
            Assert.NotSame(expected, actual);
        }

        [Fact]
        public void MyClone_FieldsAreCopied()
        {
            // Arrange
            var expected = GetFireMonster();

            // Act
            var actual = expected.MyClone();

            // Assert
            Assert.Equal(expected.Name, actual.Name);
            Assert.Equal(expected.MeleeDamage, actual.MeleeDamage);
            Assert.Equal(expected.Defence, actual.Defence);
            Assert.Equal(expected.Health, actual.Health);
            Assert.Equal(expected.MoneyDrop, actual.MoneyDrop);
            Assert.Equal(expected.FireDamage, actual.FireDamage);
        }

        [Fact]
        public void Clone_FieldsAreCopied()
        {
            // Arrange
            var expected = GetFireMonster();

            // Act
            var actual = (FireMonster)expected.Clone();

            // Assert
            Assert.Equal(expected.Name, actual.Name);
            Assert.Equal(expected.MeleeDamage, actual.MeleeDamage);
            Assert.Equal(expected.Defence, actual.Defence);
            Assert.Equal(expected.Health, actual.Health);
            Assert.Equal(expected.MoneyDrop, actual.MoneyDrop);
            Assert.Equal(expected.FireDamage, actual.FireDamage);
        }

        [Fact]
        public void MyClone_ChangeOriginObjectDoesNotChangeClonedObject()
        {
            // Arrange
            var expected = GetFireMonster();

            // Act
            var actual = expected.MyClone();
            expected.Name = "ChangedName";

            // Assert
            Assert.NotEqual(expected.Name, actual.Name);
        }

        private FireMonster GetFireMonster()
        {
            return new FireMonster
            {
                Name = "TestFireMonster",
                MeleeDamage = 5,
                Defence = 10,
                Health = 100,
                MoneyDrop = 22,
                FireDamage = 25
            };
        }
    }
}
