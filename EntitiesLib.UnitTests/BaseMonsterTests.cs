﻿using Xunit;

namespace EntitiesLib.UnitTests
{
    public class BaseMonsterTests
    {
        [Fact]
        public void MyClone_ObjectsAreNotTheSameInstance()
        {
            // Arrange
            var expected = GetBaseMonster();

            // Act
            var actual = expected.MyClone();

            // Assert
            Assert.NotSame(expected, actual);
        }

        [Fact]
        public void Clone_ObjectsAreNotTheSameInstance()
        {
            // Arrange
            var expected = GetBaseMonster();

            // Act
            var actual = expected.Clone();

            // Assert
            Assert.NotSame(expected, actual);
        }

        [Fact]
        public void MyClone_FieldsAreCopied()
        {
            // Arrange
            var expected = GetBaseMonster();

            // Act
            var actual = expected.MyClone();

            // Assert
            Assert.Equal(expected.Name, actual.Name);
            Assert.Equal(expected.MeleeDamage, actual.MeleeDamage);
            Assert.Equal(expected.Defence, actual.Defence);
            Assert.Equal(expected.Health, actual.Health);
            Assert.Equal(expected.MoneyDrop, actual.MoneyDrop);
        }

        [Fact]
        public void Clone_FieldsAreCopied()
        {
            // Arrange
            var expected = GetBaseMonster();

            // Act
            var actual = (BaseMonster)expected.Clone();

            // Assert
            Assert.Equal(expected.Name, actual.Name);
            Assert.Equal(expected.MeleeDamage, actual.MeleeDamage);
            Assert.Equal(expected.Defence, actual.Defence);
            Assert.Equal(expected.Health, actual.Health);
            Assert.Equal(expected.MoneyDrop, actual.MoneyDrop);
        }

        [Fact]
        public void MyClone_ChangeOriginObjectDoesNotChangeClonedObject()
        {
            // Arrange
            var expected = GetBaseMonster();

            // Act
            var actual = expected.MyClone();
            expected.Name = "ChangedName";

            // Assert
            Assert.NotEqual(expected.Name, actual.Name);
        }

        private BaseMonster GetBaseMonster()
        {
            return new BaseMonster
            {
                Name = "TestBaseMonster",
                MeleeDamage = 5,
                Defence = 10,
                Health = 50,
                MoneyDrop = 2
            };
        }
    }
}
