﻿using Xunit;

namespace EntitiesLib.UnitTests
{
    public  class PoisonMonsterTests
    {
        [Fact]
        public void MyClone_ObjectsAreNotTheSameInstance()
        {
            // Arrange
            var expected = GetFireMonster();

            // Act
            var actual = expected.MyClone();

            // Assert
            Assert.NotSame(expected, actual);
        }

        [Fact]
        public void Clone_ObjectsAreNotTheSameInstance()
        {
            // Arrange
            var expected = GetFireMonster();

            // Act
            var actual = expected.Clone();

            // Assert
            Assert.NotSame(expected, actual);
        }

        [Fact]
        public void MyClone_FieldsAreCopied()
        {
            // Arrange
            var expected = GetFireMonster();

            // Act
            var actual = expected.MyClone();

            // Assert
            Assert.Equal(expected.Name, actual.Name);
            Assert.Equal(expected.MeleeDamage, actual.MeleeDamage);
            Assert.Equal(expected.Defence, actual.Defence);
            Assert.Equal(expected.Health, actual.Health);
            Assert.Equal(expected.MoneyDrop, actual.MoneyDrop);
            Assert.Equal(expected.PoisonDamage, actual.PoisonDamage);
        }

        [Fact]
        public void Clone_FieldsAreCopied()
        {
            // Arrange
            var expected = GetFireMonster();

            // Act
            var actual = (PoisonMonster)expected.Clone();

            // Assert
            Assert.Equal(expected.Name, actual.Name);
            Assert.Equal(expected.MeleeDamage, actual.MeleeDamage);
            Assert.Equal(expected.Defence, actual.Defence);
            Assert.Equal(expected.Health, actual.Health);
            Assert.Equal(expected.MoneyDrop, actual.MoneyDrop);
            Assert.Equal(expected.PoisonDamage, actual.PoisonDamage);
        }

        [Fact]
        public void MyClone_ChangeOriginObjectDoesNotChangeClonedObject()
        {
            // Arrange
            var expected = GetFireMonster();

            // Act
            var actual = expected.MyClone();
            expected.Name = "ChangedName";

            // Assert
            Assert.NotEqual(expected.Name, actual.Name);
        }

        private PoisonMonster GetFireMonster()
        {
            return new PoisonMonster
            {
                Name = "TestPoisonMonster",
                MeleeDamage = 5,
                Defence = 10,
                Health = 80,
                MoneyDrop = 22,
                PoisonDamage = 55
            };
        }
    }
}
